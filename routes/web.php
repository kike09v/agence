<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('home');

Route::get('/performanceComercial', 'PerformanceComercialController@index')->name('performance');
Route::get('/performanceComercial/relatario/{modal}/load-{action}', 'PerformanceComercialController@ModalRelatario')->name('modalRelatario');
Route::get('/performanceComercial/pizza/{modal}/load-{action}', 'PerformanceComercialController@ModalPizza')->name('modalPizza');
Route::get('/performanceComercial/barra/{modal}/load-{action}', 'PerformanceComercialController@ModalBarra')->name('modalBarra');
