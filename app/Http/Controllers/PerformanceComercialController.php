<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fatura;
use DB;

class PerformanceComercialController extends Controller
{
    public function index()
    {
        $datas = DB::table('cao_usuario')
            ->join('permissao_sistema', 'cao_usuario.co_usuario', '=', 'permissao_sistema.co_usuario')
            ->where('permissao_sistema.co_sistema', '=' , 1)
            ->where('permissao_sistema.in_ativo', '=' , 'S')
            ->whereIn('permissao_sistema.co_tipo_usuario', [0,1,2])
            ->select('cao_usuario.*', 'permissao_sistema.*')
            ->get();

        return view('comercial.performanceComercial.index')->with('datas', $datas);
    }

    public function ModalRelatario(Request $request, $modal, $action)
    {        
        $countUser = count($request->array);
        $obj  = array();

        foreach($request->array as $coUser){      
  
            $os = DB::table('cao_os')->where('co_usuario', $coUser)->get();

            $caoUsuario = DB::table('cao_usuario')->where('co_usuario', $coUser)->first();
            $nameUser = $caoUsuario->no_usuario;                
            $obj2  = array();

            foreach($os as $systemsCodes){ 
                
                
                    $bills  = Fatura::where('co_sistema', $systemsCodes->co_sistema)->where('co_os', $systemsCodes->co_os)->get();

                    // $bills = DB::table('cao_fatura')->where('co_sistema', $systemsCodes->co_sistema)->get();

                for($i=0; $i<count($bills); $i++){
                    
                    if(!$bills->isEmpty()){
                        
                        $attr1 = [
                            'bill' => $bills,
                        ];
                        array_push($obj2, $attr1);
                    }
                }
            }

            $attr2 = [
                'bill' => $obj2,
                'name' => $nameUser
            ];
            array_push($obj, $attr2);


        }

        // dd($obj[0]['bill'][0]['bill'][0]->valor);
        return view('comercial.performanceComercial.relatario', compact('countUser', 'obj'));


    }

    

    public function ModalPizza(Request $request, $modal, $action)
    {
        $data = $request->array;
        
        return view('comercial.performanceComercial.pizza');
    }

    public function ModalBarra(Request $request, $modal, $action)
    {
        $data = $request->array;
        
        return view('comercial.performanceComercial.barra');
    }
}
