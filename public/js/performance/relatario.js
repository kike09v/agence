$(document).ready(function () {

    $('.relatario').click(function (e) {

        var array = [];
        $("select option:selected").each(function () {
            $data = {
                coUser: $(this).val()
            }
            array.push($data);
        });

        var this_action = $(this).attr('data-action');
        var this_sizes = $(this).attr('data-size');
        var title = $(this).attr('data-title');
        var target = $(this).attr('data-target');
        var base_url = target + '/' + this_action + '/';

        if (this_action == this_action) {
            $.get(base_url + 'load-' + this_action, {
                array
            }, function (data) {
                $('#myModal').on('shown.bs.modal', function () {
                    $('#myModal .load_modal').html(data);
                    $('.modal-header .modal-title').html(title);
                    $(this).find('.modal-dialog').addClass(this_sizes);
                });

                $('#myModal').modal("show");
                $('#myModal').on('hidden.bs.modal', function () {
                    $('#myModal .modal-body').data('');
                });
            });
        }
    });
});
