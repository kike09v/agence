@extends('welcome')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="mb-0 text-gray-800">Performance Comercial</h1>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Performance Comercial</li>
        </ol>
    </nav>
</div>
<hr>

<div class="container">
  <div class="row">
    <div class="col text-center">
      <h3>Consultores</h3>
    </div>
    <div class="col">
        <select id='pre-selected-options' multiple='multiple'>
        @foreach($datas as $data)
            <option data-co='{{$data->co_usuario}}' value='{{$data->co_usuario}}' >{{$data->no_usuario}}</option>
        @endforeach
    </select>
    </div>
    <div class="col text-center">
    <button type="button" class="btn btn-primary relatario" data-toggle="modal" data-target="performanceComercial/relatario"
        data-title="Relatario" data-size="modal-xl" data-action="modal">Relatario
        </button><br><br>

        <button type="button" class="btn btn-primary barra" data-toggle="modal" data-target="performanceComercial/barra"
        data-title="Grafico" data-size="modal-lg" data-action="modal">Grafico
        </button><br><br>


        <button type="button" class="btn btn-primary pizza" data-toggle="modal" data-target="performanceComercial/pizza"
        data-title="Pizza" data-size="modal-lg" data-action="modal">Pizza
        </button>        
    </div>
  </div>
</div>

<hr> 

@yield('table')

  

@stop

@section('js')
    <script src="{{asset('js/performance/relatario.js')}}"></script> 
    <script src="{{asset('js/performance/barra.js')}}"></script>  
    <script src="{{asset('js/performance/pizza.js')}}"></script>   
@endsection