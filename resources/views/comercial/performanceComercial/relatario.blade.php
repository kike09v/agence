<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title text-dark"></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">
@foreach($obj as $data)

<h2>{{$data['name']}}</h2>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Período</th>
      <th scope="col">Receita Líquida</th>
      <th scope="col">Custo Fixo</th>
      <th scope="col">Comissão</th>
      <th scope="col">Lucro</th>
    </tr>
  </thead>
  @for($i=0; $i<count($data['bill']); $i++)
    @foreach($data['bill'][$i] as $bill)
        @for($j=1; $j<count($bill); $j++)
        <tbody>
            <tr>
                <th scope="row">{{$j}}</th>
                <td>{{$data['name']}}</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>{{$bill[$j]->valor}}</td>
            </tr>
        </tbody>
        @endfor
    @endforeach
  @endfor
</table>
@endforeach


</div>

<div class="form-group modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</div>