<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Agence</title>

  <link href="{{ asset('vendorTemplate/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/multi-select.css')}}">

</head>

<body id="page-top">

  <div id="wrapper">

    <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: #1b2838" id="accordionSidebar">

      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-text mx-3">Bienvenido</sup></div>
      </a>

      <hr class="sidebar-divider my-0">

      @include('menu')

      <hr class="sidebar-divider my-0"><br>

      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>

    <div id="content-wrapper" class="d-flex flex-column">

      <div id="content">

        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <li class="nav-item dropdown no-arrow">
              <a class="sidebar-brand d-flex align-items-center justify-content-center" href="http://www.agence.com.br/">
                <img src="{{ asset('img/logo.gif') }}" class="img-thumbnail">
              </a>
            </li>

          </ul>

        </nav>

        <div class="container-fluid">
            <main class="container-fluid">
                @yield('content')
            </main>
        </div>

      </div>

    </div>
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- ---------------------- Modal--------------------- -->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content load_modal">

            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendorTemplate/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendorTemplate/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendorTemplate/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('vendorTemplate/chart.js/Chart.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

    <script src="{{asset('js/jquery.multi-select.js')}}"></script>
    <script type="text/javascript">
    // run pre selected options
    $('#pre-selected-options').multiSelect();
    </script>

    @yield('js')

</body>

</html>